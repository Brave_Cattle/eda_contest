#include <bits/stdc++.h>
#include "patoh.h"
using namespace std;
const int MAX_N = 700005;

int g[MAX_N], gp[MAX_N]; // map arrays

int _c = 0, _n = 0, cut, _nconst = 10, total_FPGA = 0;
vector <int> _cwghts, _nwghts, _xpins, _pins;
int total_n = 0; // total number of nodes in the net

int get_num(int num, int t){ return t ? g[num] : gp[num]; }
int get_num(char *x){
    int len = strlen(x), p = 0, tn = 0, t = 0; string ts = "";
    while(p < len && islower(x[p])) ts += x[p], p++;
    if(ts.length() == 1) t = 1;
    while(p < len && '0' <= x[p] && x[p] <= '9') tn = tn*10+x[p++]-'0';
    return get_num(tn, t);
}
int get_num(string x){
    int len = x.length(), p = 0, tn = 0, t = 0; string ts = "";
    while(p < len && islower(x[p])) ts += x[p], p++;
    if(ts.length() == 1) t = 1;
    while(p < len && '0' <= x[p] && x[p] <= '9') tn = tn*10+x[p++]-'0';
    return get_num(tn, t);
}

class Node{
    public:
        int v[12], c[5], ff, num, gt;
        // gt == 0 -> g, gt == 1 -> gp

        void input(char *line){
            ff = num = gt = 0, memset(v, 0, sizeof(v)), memset(c, 0, sizeof(c));

            int len, p = 0, tl; string ts = ""; gt = line[1] == 'p' ? 0 : 1;
            len = strlen(line);
            while(p < len && islower(line[p])) p++;
            while(p < len && '0' <= line[p] && line[p] <= '9') num = num*10+line[p++]-'0';
            if(gt) g[num] = _c;
            else gp[num] = _c;

            for(int i = 1; i <= 10 && p < len; i++){
                while(line[p] < '0' || line[p] > '9') p++;
                while(p < len && '0' <= line[p] && line[p] <= '9')
                    v[i] = v[i]*10+line[p++]-'0';
            }
            for(int i = 1; i <= 10; i++) _cwghts.push_back(v[i]);

            while(p < len && !islower(line[p])) p++;
            while(p < len && line[p] != ' ') ts += line[p], p++;
            if(tl = ts.length()){
                if(ts[0] == 'c'){
                    for(int i = 0; i < tl; i++)
                        if('0' <= ts[i] && ts[i] <= '9') c[ts[i]-'0'] = 1;
                } else ff = 1;
            }
            ts = "";
            while(p < len && !islower(line[p])) p++;
            while(p < len && line[p] != ' ') ts += line[p], p++;
            if(tl = ts.length())
                for(int i = 0; i < tl; i++)
                    if('0' <= ts[i] && ts[i] <= '9') c[ts[i]-'0'] = 1;
        }

        void print(){
            printf("node:");
            if(gt) printf("g%d real_num=%d ", num, get_num(num, gt));
            else printf("gp%d read_num=%d ", num, get_num(num, gt));
            printf("ff=%d c0=%d c1=%d c2=%d c3=%d\n", ff, c[0], c[1], c[2], c[3]);
            for(int i = 1; i <= 10; i++) printf("v[%d]=%d ", i, v[i]); printf("\n");
        }

        void check_input(){
            if(gt) printf("g"); else printf("gp");
            printf("%d ", num);
            for(int i = 1; i <= 10; i++)
                if(i != 10) printf("%d ", v[i]);
                else printf("%d", v[i]);
            if(ff || c[0] || c[1] || c[2] || c[3]){
                printf(" {");
                if(ff){
                    if(!c[0] && !c[1] && !c[2] && !c[3]) printf("ff}\n");
                    else {
                        printf("ff ");
                        for(int i = 0; i <= 3; i++)
                            if(c[i]) printf("c%d", i);
                        printf("}\n");
                    }
                } else {
                    for(int i = 0; i <= 3; i++)
                        if(c[i]) printf("c%d", i);
                    printf("}\n");
                }
            } else printf("\n");
        }

}tmp_node;

vector <Node> a;

class FPGA{
    public:
        int v[12], list[20], flag;

        void input(char *line){
            memset(v, 0, sizeof(v)), memset(list, 0, sizeof(list)); flag = 0;
            int len, p = 4; len = strlen(line);
            while(p < len && line[p] < '0' || '9' < line[p]) p++;
            for(int i = 1; i <= 10 && p < len; i++){
                while(line[p] < '0' || line[p] > '9') p++;
                while(p < len && '0' <= line[p] && line[p] <= '9')
                    v[i] = v[i]*10+line[p++]-'0';
            }
            while(p < len && line[p] < '0' || '9' < line[p]) p++;
            if(p < len){
                flag = 1;
                for(int i = 1; i <= 10 && p < len; i++){
                    while(line[p] < '0' || line[p] > '9') p++;
                    while(p < len && '0' <= line[p] && line[p] <= '9')
                        list[i] = list[i]*10+line[p++]-'0';
                }
            }
        }

        void check_input(){
            printf("FPGA ");
            for(int i = 1; i <= 10; i++) printf("%d ", v[i]);
            if(flag){
                printf("{ ");
                for(int i = 1; i <= 10; i++) printf("%d ", list[i]);
                printf("} \n");
            } else printf("\n");
        }
}f[20];

vector <int> fix[20];

void Graph_input(){
    char node_name[15], type[3]; int w;
    while(scanf("%s%s", node_name, type) != EOF){
        _pins.push_back(get_num(node_name)); total_n++;
        if(type[0] == 's'){
            _xpins.push_back(total_n-1), _nwghts.push_back(w); _n++;
            scanf("%d", &w);
        }
    }
    _nwghts.push_back(w), _xpins.push_back(total_n);
}

char line[200];

int main(int argc, char *argv[]){
    freopen("C:/Users/wnjf/Desktop/files/eda_contest/benchmark/testdata-3/design.are", "r", stdin);
    // freopen("test.out", "w", stdout);
    while(fgets(line, sizeof(line), stdin)){
        tmp_node.input(line);
        a.push_back(tmp_node); _c++;
    }
    // for(int i = 0; i < _c; i++) a[i].check_input();
    
    freopen("C:/Users/wnjf/Desktop/files/eda_contest/benchmark/testdata-3/design.net", "r", stdin);
    Graph_input();
    int *cwghts{ new int[_c*_nconst+1]{} }, *nwghts{ new int[_n+1]{} },
        *xpins{ new int[_n+2]{} }, *pins{ new int[total_n+1]{} },
        *partvec{ new int[_c+1]{} };
    for(int i = 0; i < _cwghts.size(); i++) cwghts[i] = _cwghts[i];
    for(int i = 0; i < _nwghts.size(); i++) nwghts[i] = _nwghts[i];
    for(int i = 0; i < _xpins.size(); i++) xpins[i] = _xpins[i];
    for(int i; i < _pins.size(); i++) pins[i] = _pins[i];
    for(int i = 0; i < _c; i++) partvec[i] = -1;
    
    freopen("C:/Users/wnjf/Desktop/files/eda_contest/benchmark/testdata-3/design.info", "r", stdin);
    while(fgets(line, sizeof(line), stdin)) f[total_FPGA++].input(line);

    freopen("C:/Users/wnjf/Desktop/files/eda_contest/benchmark/testdata-3/design.fix", "r", stdin);
    while(fgets(line, sizeof(line), stdin)){
        int len = strlen(line), p = 0, num = 0; string ts = "";
        while(p < len && (line[p] < '0' || line[p] > '9')) p++;
        while(p < len && '0' <= line[p] && line[p] <= '9') num = num*10+line[p++]-'0';
        num--;
        while(p < len){
            ts = "";
            while(p < len && !islower(line[p])) p++;
            while(p < len && (islower(line[p]) || ('0' <= line[p] && line[p] <= '9')))
                ts += line[p], p++;
            if(p < len && ts.length()) partvec[get_num(ts)] = num;
        }
    }

    // for(int i = 0; i < total_FPGA; i++) f[i].check_input();

    PaToH_Parameters args;
    
    printf("Hypergraph %10s -- #Cells=%6d  #Nets=%6d  #Pins=%8d #Const=%2d\n",
           argv[1], _c, _n, xpins[_n], _nconst);
    /*
    PaToH_Initialize_Parameters(&args, PATOH_CONPART, PATOH_SUGPARAM_DEFAULT);
    args._k = atoi(argv[2]);
    int *partweights{ new int[args._k*_nconst+1]{} };

    PaToH_Alloc(&args, _c, _n, _nconst, cwghts, nwghts, xpins, pins);
    // PaToH_Part(&args, _c, _n, _nconst, 1, cwghts, nwghts, xpins, pins, NULL, partvec, partweights, &cut);

    printf("%d-way cutsize is: %d\n", args._k, cut);
    */
    delete[] cwghts; delete[] nwghts; delete[] xpins; delete[] pins;
    delete[] partvec;
    // delete[] partweights;
    // PaToH_Free();
    
    return 0;
}